﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practica
{
    class Constante
    {
        public enum TipuriUtilizator { indrumator = 1, student = 2 };
        static public List<string> listaUtilizatori = new List<string>();
        static public List<int> listaNote = new List<int>();
        static public List<int> listaAn = new List<int>(); 
        public static int studentLogat;
    }
}
