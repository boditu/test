﻿namespace practica
{
    partial class StudentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.utilizator = new System.Windows.Forms.TabControl();
            this.utilizatorTab = new System.Windows.Forms.TabPage();
            this.listaTaskuri = new System.Windows.Forms.ListBox();
            this.adaugaTaskNou = new System.Windows.Forms.Button();
            this.taskNou = new System.Windows.Forms.TextBox();
            this.status = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.toateStatusurile = new System.Windows.Forms.ComboBox();
            this.timpTotal = new System.Windows.Forms.TextBox();
            this.timpActual = new System.Windows.Forms.TextBox();
            this.stop = new System.Windows.Forms.Button();
            this.start = new System.Windows.Forms.Button();
            this.comentarii = new System.Windows.Forms.TextBox();
            this.adaugaNotite = new System.Windows.Forms.Button();
            this.notiteTask = new System.Windows.Forms.TextBox();
            this.user = new System.Windows.Forms.TabPage();
            this.updateStudent = new System.Windows.Forms.Button();
            this.parolaLabel = new System.Windows.Forms.Label();
            this.parolaNoua = new System.Windows.Forms.TextBox();
            this.mail = new System.Windows.Forms.TextBox();
            this.nota = new System.Windows.Forms.TextBox();
            this.an = new System.Windows.Forms.TextBox();
            this.institutie = new System.Windows.Forms.TextBox();
            this.adresa = new System.Windows.Forms.TextBox();
            this.cnp = new System.Windows.Forms.TextBox();
            this.nume = new System.Windows.Forms.TextBox();
            this.mailLabel = new System.Windows.Forms.Label();
            this.notaLabel = new System.Windows.Forms.Label();
            this.anLabel = new System.Windows.Forms.Label();
            this.institutieLabel = new System.Windows.Forms.Label();
            this.adresaLabel = new System.Windows.Forms.Label();
            this.cnpLabel = new System.Windows.Forms.Label();
            this.numeLabel = new System.Windows.Forms.Label();
            this.rapoarte = new System.Windows.Forms.TabPage();
            this.printeazaRaportSaptamanal = new System.Windows.Forms.Button();
            this.printeazaRaportTotal = new System.Windows.Forms.Button();
            this.DataTable1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.utilizator.SuspendLayout();
            this.utilizatorTab.SuspendLayout();
            this.user.SuspendLayout();
            this.rapoarte.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataTable1BindingSource)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // utilizator
            // 
            this.utilizator.Controls.Add(this.utilizatorTab);
            this.utilizator.Controls.Add(this.user);
            this.utilizator.Controls.Add(this.rapoarte);
            this.utilizator.Location = new System.Drawing.Point(14, 15);
            this.utilizator.Name = "utilizator";
            this.utilizator.SelectedIndex = 0;
            this.utilizator.Size = new System.Drawing.Size(492, 349);
            this.utilizator.TabIndex = 0;
            // 
            // utilizatorTab
            // 
            this.utilizatorTab.Controls.Add(this.listaTaskuri);
            this.utilizatorTab.Controls.Add(this.adaugaTaskNou);
            this.utilizatorTab.Controls.Add(this.taskNou);
            this.utilizatorTab.Controls.Add(this.status);
            this.utilizatorTab.Controls.Add(this.label1);
            this.utilizatorTab.Controls.Add(this.toateStatusurile);
            this.utilizatorTab.Controls.Add(this.timpTotal);
            this.utilizatorTab.Controls.Add(this.timpActual);
            this.utilizatorTab.Controls.Add(this.stop);
            this.utilizatorTab.Controls.Add(this.start);
            this.utilizatorTab.Controls.Add(this.comentarii);
            this.utilizatorTab.Controls.Add(this.adaugaNotite);
            this.utilizatorTab.Controls.Add(this.notiteTask);
            this.utilizatorTab.Location = new System.Drawing.Point(4, 22);
            this.utilizatorTab.Name = "utilizatorTab";
            this.utilizatorTab.Padding = new System.Windows.Forms.Padding(3);
            this.utilizatorTab.Size = new System.Drawing.Size(484, 323);
            this.utilizatorTab.TabIndex = 0;
            this.utilizatorTab.Text = "Task";
            this.utilizatorTab.UseVisualStyleBackColor = true;
            // 
            // listaTaskuri
            // 
            this.listaTaskuri.FormattingEnabled = true;
            this.listaTaskuri.Location = new System.Drawing.Point(16, 16);
            this.listaTaskuri.Name = "listaTaskuri";
            this.listaTaskuri.Size = new System.Drawing.Size(141, 186);
            this.listaTaskuri.TabIndex = 13;
            this.listaTaskuri.SelectedIndexChanged += new System.EventHandler(this.listaTaskuri_SelectedIndexChanged);
            // 
            // adaugaTaskNou
            // 
            this.adaugaTaskNou.Location = new System.Drawing.Point(16, 268);
            this.adaugaTaskNou.Name = "adaugaTaskNou";
            this.adaugaTaskNou.Size = new System.Drawing.Size(100, 25);
            this.adaugaTaskNou.TabIndex = 12;
            this.adaugaTaskNou.Text = "Adauga task";
            this.adaugaTaskNou.UseVisualStyleBackColor = true;
            this.adaugaTaskNou.Click += new System.EventHandler(this.adaugaTask_Click);
            // 
            // taskNou
            // 
            this.taskNou.Location = new System.Drawing.Point(16, 242);
            this.taskNou.Name = "taskNou";
            this.taskNou.Size = new System.Drawing.Size(100, 20);
            this.taskNou.TabIndex = 11;
            // 
            // status
            // 
            this.status.Enabled = false;
            this.status.Location = new System.Drawing.Point(192, 153);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(126, 20);
            this.status.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(231, 127);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Status actual";
            // 
            // toateStatusurile
            // 
            this.toateStatusurile.FormattingEnabled = true;
            this.toateStatusurile.Location = new System.Drawing.Point(353, 153);
            this.toateStatusurile.Name = "toateStatusurile";
            this.toateStatusurile.Size = new System.Drawing.Size(92, 21);
            this.toateStatusurile.TabIndex = 8;
            this.toateStatusurile.SelectedIndexChanged += new System.EventHandler(this.toateStatusurile_SelectedIndexChanged);
            // 
            // timpTotal
            // 
            this.timpTotal.Enabled = false;
            this.timpTotal.Location = new System.Drawing.Point(265, 214);
            this.timpTotal.Name = "timpTotal";
            this.timpTotal.Size = new System.Drawing.Size(53, 20);
            this.timpTotal.TabIndex = 7;
            // 
            // timpActual
            // 
            this.timpActual.Enabled = false;
            this.timpActual.Location = new System.Drawing.Point(192, 214);
            this.timpActual.Name = "timpActual";
            this.timpActual.Size = new System.Drawing.Size(53, 20);
            this.timpActual.TabIndex = 6;
            // 
            // stop
            // 
            this.stop.Location = new System.Drawing.Point(265, 182);
            this.stop.Name = "stop";
            this.stop.Size = new System.Drawing.Size(53, 26);
            this.stop.TabIndex = 5;
            this.stop.Text = "Stop";
            this.stop.UseVisualStyleBackColor = true;
            this.stop.Click += new System.EventHandler(this.stop_Click);
            // 
            // start
            // 
            this.start.Location = new System.Drawing.Point(192, 182);
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(53, 26);
            this.start.TabIndex = 4;
            this.start.Text = "Start";
            this.start.UseVisualStyleBackColor = true;
            this.start.Click += new System.EventHandler(this.start_Click);
            // 
            // comentarii
            // 
            this.comentarii.Enabled = false;
            this.comentarii.Location = new System.Drawing.Point(192, 240);
            this.comentarii.Multiline = true;
            this.comentarii.Name = "comentarii";
            this.comentarii.Size = new System.Drawing.Size(253, 53);
            this.comentarii.TabIndex = 3;
            // 
            // adaugaNotite
            // 
            this.adaugaNotite.Location = new System.Drawing.Point(353, 119);
            this.adaugaNotite.Name = "adaugaNotite";
            this.adaugaNotite.Size = new System.Drawing.Size(92, 28);
            this.adaugaNotite.TabIndex = 2;
            this.adaugaNotite.Text = "Adauga notite";
            this.adaugaNotite.UseVisualStyleBackColor = true;
            this.adaugaNotite.Click += new System.EventHandler(this.adaugaNotite_Click);
            // 
            // notiteTask
            // 
            this.notiteTask.Location = new System.Drawing.Point(192, 16);
            this.notiteTask.Multiline = true;
            this.notiteTask.Name = "notiteTask";
            this.notiteTask.Size = new System.Drawing.Size(253, 97);
            this.notiteTask.TabIndex = 1;
            // 
            // user
            // 
            this.user.Controls.Add(this.updateStudent);
            this.user.Controls.Add(this.parolaLabel);
            this.user.Controls.Add(this.parolaNoua);
            this.user.Controls.Add(this.mail);
            this.user.Controls.Add(this.nota);
            this.user.Controls.Add(this.an);
            this.user.Controls.Add(this.institutie);
            this.user.Controls.Add(this.adresa);
            this.user.Controls.Add(this.cnp);
            this.user.Controls.Add(this.nume);
            this.user.Controls.Add(this.mailLabel);
            this.user.Controls.Add(this.notaLabel);
            this.user.Controls.Add(this.anLabel);
            this.user.Controls.Add(this.institutieLabel);
            this.user.Controls.Add(this.adresaLabel);
            this.user.Controls.Add(this.cnpLabel);
            this.user.Controls.Add(this.numeLabel);
            this.user.Location = new System.Drawing.Point(4, 22);
            this.user.Name = "user";
            this.user.Padding = new System.Windows.Forms.Padding(3);
            this.user.Size = new System.Drawing.Size(484, 323);
            this.user.TabIndex = 1;
            this.user.Text = "User";
            this.user.UseVisualStyleBackColor = true;
            // 
            // updateStudent
            // 
            this.updateStudent.Location = new System.Drawing.Point(324, 269);
            this.updateStudent.Name = "updateStudent";
            this.updateStudent.Size = new System.Drawing.Size(90, 24);
            this.updateStudent.TabIndex = 43;
            this.updateStudent.Text = "Update";
            this.updateStudent.UseVisualStyleBackColor = true;
            this.updateStudent.Click += new System.EventHandler(this.button4_Click);
            // 
            // parolaLabel
            // 
            this.parolaLabel.AutoSize = true;
            this.parolaLabel.Location = new System.Drawing.Point(53, 280);
            this.parolaLabel.Name = "parolaLabel";
            this.parolaLabel.Size = new System.Drawing.Size(37, 13);
            this.parolaLabel.TabIndex = 37;
            this.parolaLabel.Text = "Parola";
            // 
            // parolaNoua
            // 
            this.parolaNoua.Location = new System.Drawing.Point(138, 273);
            this.parolaNoua.Name = "parolaNoua";
            this.parolaNoua.Size = new System.Drawing.Size(100, 20);
            this.parolaNoua.TabIndex = 36;
            this.parolaNoua.UseSystemPasswordChar = true;
            // 
            // mail
            // 
            this.mail.Location = new System.Drawing.Point(138, 240);
            this.mail.Name = "mail";
            this.mail.Size = new System.Drawing.Size(100, 20);
            this.mail.TabIndex = 34;
            // 
            // nota
            // 
            this.nota.Location = new System.Drawing.Point(138, 203);
            this.nota.MaxLength = 2;
            this.nota.Name = "nota";
            this.nota.Size = new System.Drawing.Size(100, 20);
            this.nota.TabIndex = 32;
            // 
            // an
            // 
            this.an.Location = new System.Drawing.Point(138, 163);
            this.an.MaxLength = 1;
            this.an.Name = "an";
            this.an.Size = new System.Drawing.Size(100, 20);
            this.an.TabIndex = 30;
            // 
            // institutie
            // 
            this.institutie.Location = new System.Drawing.Point(138, 121);
            this.institutie.Name = "institutie";
            this.institutie.Size = new System.Drawing.Size(100, 20);
            this.institutie.TabIndex = 28;
            // 
            // adresa
            // 
            this.adresa.Location = new System.Drawing.Point(138, 80);
            this.adresa.Name = "adresa";
            this.adresa.Size = new System.Drawing.Size(100, 20);
            this.adresa.TabIndex = 26;
            // 
            // cnp
            // 
            this.cnp.Location = new System.Drawing.Point(138, 42);
            this.cnp.MaxLength = 13;
            this.cnp.Name = "cnp";
            this.cnp.Size = new System.Drawing.Size(100, 20);
            this.cnp.TabIndex = 24;
            // 
            // nume
            // 
            this.nume.Location = new System.Drawing.Point(138, 6);
            this.nume.Name = "nume";
            this.nume.Size = new System.Drawing.Size(100, 20);
            this.nume.TabIndex = 22;
            // 
            // mailLabel
            // 
            this.mailLabel.AutoSize = true;
            this.mailLabel.Location = new System.Drawing.Point(53, 240);
            this.mailLabel.Name = "mailLabel";
            this.mailLabel.Size = new System.Drawing.Size(26, 13);
            this.mailLabel.TabIndex = 35;
            this.mailLabel.Text = "Mail";
            // 
            // notaLabel
            // 
            this.notaLabel.AutoSize = true;
            this.notaLabel.Location = new System.Drawing.Point(53, 203);
            this.notaLabel.Name = "notaLabel";
            this.notaLabel.Size = new System.Drawing.Size(30, 13);
            this.notaLabel.TabIndex = 33;
            this.notaLabel.Text = "Nota";
            // 
            // anLabel
            // 
            this.anLabel.AutoSize = true;
            this.anLabel.Location = new System.Drawing.Point(53, 163);
            this.anLabel.Name = "anLabel";
            this.anLabel.Size = new System.Drawing.Size(20, 13);
            this.anLabel.TabIndex = 31;
            this.anLabel.Text = "An";
            // 
            // institutieLabel
            // 
            this.institutieLabel.AutoSize = true;
            this.institutieLabel.Location = new System.Drawing.Point(53, 121);
            this.institutieLabel.Name = "institutieLabel";
            this.institutieLabel.Size = new System.Drawing.Size(55, 13);
            this.institutieLabel.TabIndex = 29;
            this.institutieLabel.Text = "Institututie";
            // 
            // adresaLabel
            // 
            this.adresaLabel.AutoSize = true;
            this.adresaLabel.Location = new System.Drawing.Point(53, 80);
            this.adresaLabel.Name = "adresaLabel";
            this.adresaLabel.Size = new System.Drawing.Size(40, 13);
            this.adresaLabel.TabIndex = 27;
            this.adresaLabel.Text = "Adresa";
            // 
            // cnpLabel
            // 
            this.cnpLabel.AutoSize = true;
            this.cnpLabel.Location = new System.Drawing.Point(53, 49);
            this.cnpLabel.Name = "cnpLabel";
            this.cnpLabel.Size = new System.Drawing.Size(29, 13);
            this.cnpLabel.TabIndex = 25;
            this.cnpLabel.Text = "CNP";
            // 
            // numeLabel
            // 
            this.numeLabel.AutoSize = true;
            this.numeLabel.Location = new System.Drawing.Point(53, 9);
            this.numeLabel.Name = "numeLabel";
            this.numeLabel.Size = new System.Drawing.Size(35, 13);
            this.numeLabel.TabIndex = 23;
            this.numeLabel.Text = "Nume";
            // 
            // rapoarte
            // 
            this.rapoarte.Controls.Add(this.printeazaRaportSaptamanal);
            this.rapoarte.Controls.Add(this.printeazaRaportTotal);
            this.rapoarte.Location = new System.Drawing.Point(4, 22);
            this.rapoarte.Name = "rapoarte";
            this.rapoarte.Padding = new System.Windows.Forms.Padding(3);
            this.rapoarte.Size = new System.Drawing.Size(484, 323);
            this.rapoarte.TabIndex = 2;
            this.rapoarte.Text = "Rapoarte";
            this.rapoarte.UseVisualStyleBackColor = true;
            // 
            // printeazaRaportSaptamanal
            // 
            this.printeazaRaportSaptamanal.Location = new System.Drawing.Point(157, 101);
            this.printeazaRaportSaptamanal.Name = "printeazaRaportSaptamanal";
            this.printeazaRaportSaptamanal.Size = new System.Drawing.Size(166, 30);
            this.printeazaRaportSaptamanal.TabIndex = 1;
            this.printeazaRaportSaptamanal.Text = "Printeaza raport saptamanal";
            this.printeazaRaportSaptamanal.UseVisualStyleBackColor = true;
            this.printeazaRaportSaptamanal.Click += new System.EventHandler(this.printeazaRaportSaptamanal_Click);
            // 
            // printeazaRaportTotal
            // 
            this.printeazaRaportTotal.Location = new System.Drawing.Point(157, 185);
            this.printeazaRaportTotal.Name = "printeazaRaportTotal";
            this.printeazaRaportTotal.Size = new System.Drawing.Size(166, 30);
            this.printeazaRaportTotal.TabIndex = 0;
            this.printeazaRaportTotal.Text = "Printeaza raport total";
            this.printeazaRaportTotal.UseVisualStyleBackColor = true;
            this.printeazaRaportTotal.Click += new System.EventHandler(this.printeazaRaportTotal_Click);
            // 
            // DataTable1BindingSource
            // 
            this.DataTable1BindingSource.DataMember = "DataTable1";
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.Controls.Add(this.utilizator);
            this.panel1.Location = new System.Drawing.Point(46, 38);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(527, 377);
            this.panel1.TabIndex = 1;
            // 
            // StudentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(623, 444);
            this.Controls.Add(this.panel1);
            this.MaximumSize = new System.Drawing.Size(800, 600);
            this.MinimumSize = new System.Drawing.Size(600, 440);
            this.Name = "StudentForm";
            this.Text = "Student";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.StudentForm_FormClosed);
            this.Load += new System.EventHandler(this.studentForm_Load);
            this.utilizator.ResumeLayout(false);
            this.utilizatorTab.ResumeLayout(false);
            this.utilizatorTab.PerformLayout();
            this.user.ResumeLayout(false);
            this.user.PerformLayout();
            this.rapoarte.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataTable1BindingSource)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl utilizator;
        private System.Windows.Forms.TabPage utilizatorTab;
        private System.Windows.Forms.TabPage user;
        private System.Windows.Forms.Button adaugaNotite;
        private System.Windows.Forms.TextBox notiteTask;
        private System.Windows.Forms.Button stop;
        private System.Windows.Forms.Button start;
        private System.Windows.Forms.TextBox timpTotal;
        private System.Windows.Forms.TextBox timpActual;
        private System.Windows.Forms.ComboBox toateStatusurile;
        private System.Windows.Forms.TextBox status;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button adaugaTaskNou;
        private System.Windows.Forms.TextBox taskNou;
        private System.Windows.Forms.TabPage rapoarte;
        private System.Windows.Forms.Button printeazaRaportTotal;
        private System.Windows.Forms.Button printeazaRaportSaptamanal;
        private System.Windows.Forms.Label parolaLabel;
        private System.Windows.Forms.TextBox parolaNoua;
        private System.Windows.Forms.TextBox mail;
        private System.Windows.Forms.TextBox nota;
        private System.Windows.Forms.TextBox an;
        private System.Windows.Forms.TextBox institutie;
        private System.Windows.Forms.TextBox adresa;
        private System.Windows.Forms.TextBox cnp;
        private System.Windows.Forms.TextBox nume;
        private System.Windows.Forms.Label mailLabel;
        private System.Windows.Forms.Label notaLabel;
        private System.Windows.Forms.Label anLabel;
        private System.Windows.Forms.Label institutieLabel;
        private System.Windows.Forms.Label adresaLabel;
        private System.Windows.Forms.Label cnpLabel;
        private System.Windows.Forms.Label numeLabel;
        private System.Windows.Forms.Button updateStudent;
        private System.Windows.Forms.BindingSource DataTable1BindingSource;
        private System.Windows.Forms.ListBox listaTaskuri;
        private System.Windows.Forms.TextBox comentarii;
        private System.Windows.Forms.Panel panel1;



    }
}