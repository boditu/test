﻿namespace practica
{
    partial class IndrumatorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.usersTab = new System.Windows.Forms.TabPage();
            this.anComboBox = new System.Windows.Forms.ComboBox();
            this.notaComboBox = new System.Windows.Forms.ComboBox();
            this.rolUtilizatorComboBox = new System.Windows.Forms.ComboBox();
            this.deleteStudent = new System.Windows.Forms.Button();
            this.updateStudent = new System.Windows.Forms.Button();
            this.parolaLabel = new System.Windows.Forms.Label();
            this.parolaNoua = new System.Windows.Forms.TextBox();
            this.mail = new System.Windows.Forms.TextBox();
            this.institutie = new System.Windows.Forms.TextBox();
            this.adresa = new System.Windows.Forms.TextBox();
            this.cnp = new System.Windows.Forms.TextBox();
            this.nume = new System.Windows.Forms.TextBox();
            this.mailLabel = new System.Windows.Forms.Label();
            this.adaugaStudent = new System.Windows.Forms.Button();
            this.notaLabel = new System.Windows.Forms.Label();
            this.anLabel = new System.Windows.Forms.Label();
            this.institutieLabel = new System.Windows.Forms.Label();
            this.adresaLabel = new System.Windows.Forms.Label();
            this.cnpLabel = new System.Windows.Forms.Label();
            this.numeLabel = new System.Windows.Forms.Label();
            this.rolLabel = new System.Windows.Forms.Label();
            this.tasks = new System.Windows.Forms.TabPage();
            this.listaTaskuri = new System.Windows.Forms.ListBox();
            this.deleteTask = new System.Windows.Forms.Button();
            this.adaugaComentarii = new System.Windows.Forms.Button();
            this.comentarii = new System.Windows.Forms.TextBox();
            this.notiteStudent = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.adaugaTask = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.taskNou = new System.Windows.Forms.TextBox();
            this.tasksTab = new System.Windows.Forms.TabControl();
            this.rapoarteTab = new System.Windows.Forms.TabPage();
            this.raportTotiStudentii = new System.Windows.Forms.Button();
            this.DataSetPractica = new practica.DataSetPractica();
            this.DataTable1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.DataTable1TableAdapter = new practica.DataSetPracticaTableAdapters.DataTable1TableAdapter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.studentiDataGridView = new System.Windows.Forms.DataGridView();
            this.exportaUtilizatoriButton = new System.Windows.Forms.Button();
            this.importaUtilizatoriButton = new System.Windows.Forms.Button();
            this.usersTab.SuspendLayout();
            this.tasks.SuspendLayout();
            this.tasksTab.SuspendLayout();
            this.rapoarteTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataSetPractica)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataTable1BindingSource)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.studentiDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // usersTab
            // 
            this.usersTab.Controls.Add(this.importaUtilizatoriButton);
            this.usersTab.Controls.Add(this.exportaUtilizatoriButton);
            this.usersTab.Controls.Add(this.anComboBox);
            this.usersTab.Controls.Add(this.notaComboBox);
            this.usersTab.Controls.Add(this.rolUtilizatorComboBox);
            this.usersTab.Controls.Add(this.deleteStudent);
            this.usersTab.Controls.Add(this.updateStudent);
            this.usersTab.Controls.Add(this.parolaLabel);
            this.usersTab.Controls.Add(this.parolaNoua);
            this.usersTab.Controls.Add(this.mail);
            this.usersTab.Controls.Add(this.institutie);
            this.usersTab.Controls.Add(this.adresa);
            this.usersTab.Controls.Add(this.cnp);
            this.usersTab.Controls.Add(this.nume);
            this.usersTab.Controls.Add(this.mailLabel);
            this.usersTab.Controls.Add(this.adaugaStudent);
            this.usersTab.Controls.Add(this.notaLabel);
            this.usersTab.Controls.Add(this.anLabel);
            this.usersTab.Controls.Add(this.institutieLabel);
            this.usersTab.Controls.Add(this.adresaLabel);
            this.usersTab.Controls.Add(this.cnpLabel);
            this.usersTab.Controls.Add(this.numeLabel);
            this.usersTab.Controls.Add(this.rolLabel);
            this.usersTab.Location = new System.Drawing.Point(4, 22);
            this.usersTab.Name = "usersTab";
            this.usersTab.Padding = new System.Windows.Forms.Padding(3);
            this.usersTab.Size = new System.Drawing.Size(429, 408);
            this.usersTab.TabIndex = 1;
            this.usersTab.Text = "Users";
            this.usersTab.UseVisualStyleBackColor = true;
            // 
            // anComboBox
            // 
            this.anComboBox.FormattingEnabled = true;
            this.anComboBox.Location = new System.Drawing.Point(127, 209);
            this.anComboBox.Name = "anComboBox";
            this.anComboBox.Size = new System.Drawing.Size(100, 21);
            this.anComboBox.TabIndex = 47;
            // 
            // notaComboBox
            // 
            this.notaComboBox.FormattingEnabled = true;
            this.notaComboBox.Location = new System.Drawing.Point(127, 249);
            this.notaComboBox.Name = "notaComboBox";
            this.notaComboBox.Size = new System.Drawing.Size(100, 21);
            this.notaComboBox.TabIndex = 46;
            // 
            // rolUtilizatorComboBox
            // 
            this.rolUtilizatorComboBox.FormattingEnabled = true;
            this.rolUtilizatorComboBox.Location = new System.Drawing.Point(127, 60);
            this.rolUtilizatorComboBox.Name = "rolUtilizatorComboBox";
            this.rolUtilizatorComboBox.Size = new System.Drawing.Size(100, 21);
            this.rolUtilizatorComboBox.TabIndex = 45;
            // 
            // deleteStudent
            // 
            this.deleteStudent.Location = new System.Drawing.Point(305, 368);
            this.deleteStudent.Name = "deleteStudent";
            this.deleteStudent.Size = new System.Drawing.Size(85, 24);
            this.deleteStudent.TabIndex = 43;
            this.deleteStudent.Text = "Delete";
            this.deleteStudent.UseVisualStyleBackColor = true;
            this.deleteStudent.Click += new System.EventHandler(this.deleteStudent_Click);
            // 
            // updateStudent
            // 
            this.updateStudent.Location = new System.Drawing.Point(180, 368);
            this.updateStudent.Name = "updateStudent";
            this.updateStudent.Size = new System.Drawing.Size(90, 24);
            this.updateStudent.TabIndex = 42;
            this.updateStudent.Text = "Update";
            this.updateStudent.UseVisualStyleBackColor = true;
            this.updateStudent.Click += new System.EventHandler(this.button4_Click);
            // 
            // parolaLabel
            // 
            this.parolaLabel.AutoSize = true;
            this.parolaLabel.Location = new System.Drawing.Point(45, 328);
            this.parolaLabel.Name = "parolaLabel";
            this.parolaLabel.Size = new System.Drawing.Size(37, 13);
            this.parolaLabel.TabIndex = 19;
            this.parolaLabel.Text = "Parola";
            // 
            // parolaNoua
            // 
            this.parolaNoua.Location = new System.Drawing.Point(127, 325);
            this.parolaNoua.Name = "parolaNoua";
            this.parolaNoua.Size = new System.Drawing.Size(100, 20);
            this.parolaNoua.TabIndex = 18;
            this.parolaNoua.UseSystemPasswordChar = true;
            // 
            // mail
            // 
            this.mail.Location = new System.Drawing.Point(127, 286);
            this.mail.Name = "mail";
            this.mail.Size = new System.Drawing.Size(100, 20);
            this.mail.TabIndex = 15;
            // 
            // institutie
            // 
            this.institutie.Location = new System.Drawing.Point(127, 167);
            this.institutie.Name = "institutie";
            this.institutie.Size = new System.Drawing.Size(100, 20);
            this.institutie.TabIndex = 8;
            // 
            // adresa
            // 
            this.adresa.Location = new System.Drawing.Point(127, 126);
            this.adresa.Name = "adresa";
            this.adresa.Size = new System.Drawing.Size(100, 20);
            this.adresa.TabIndex = 6;
            // 
            // cnp
            // 
            this.cnp.Location = new System.Drawing.Point(127, 88);
            this.cnp.MaxLength = 13;
            this.cnp.Name = "cnp";
            this.cnp.Size = new System.Drawing.Size(100, 20);
            this.cnp.TabIndex = 4;
            // 
            // nume
            // 
            this.nume.Location = new System.Drawing.Point(127, 22);
            this.nume.Name = "nume";
            this.nume.Size = new System.Drawing.Size(100, 20);
            this.nume.TabIndex = 2;
            // 
            // mailLabel
            // 
            this.mailLabel.AutoSize = true;
            this.mailLabel.Location = new System.Drawing.Point(45, 289);
            this.mailLabel.Name = "mailLabel";
            this.mailLabel.Size = new System.Drawing.Size(26, 13);
            this.mailLabel.TabIndex = 16;
            this.mailLabel.Text = "Mail";
            // 
            // adaugaStudent
            // 
            this.adaugaStudent.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.adaugaStudent.Location = new System.Drawing.Point(45, 368);
            this.adaugaStudent.Name = "adaugaStudent";
            this.adaugaStudent.Size = new System.Drawing.Size(96, 24);
            this.adaugaStudent.TabIndex = 14;
            this.adaugaStudent.Text = "Adauga student";
            this.adaugaStudent.UseVisualStyleBackColor = true;
            this.adaugaStudent.Click += new System.EventHandler(this.button_adauga_student_Click);
            // 
            // notaLabel
            // 
            this.notaLabel.AutoSize = true;
            this.notaLabel.Location = new System.Drawing.Point(45, 252);
            this.notaLabel.Name = "notaLabel";
            this.notaLabel.Size = new System.Drawing.Size(30, 13);
            this.notaLabel.TabIndex = 13;
            this.notaLabel.Text = "Nota";
            // 
            // anLabel
            // 
            this.anLabel.AutoSize = true;
            this.anLabel.Location = new System.Drawing.Point(45, 212);
            this.anLabel.Name = "anLabel";
            this.anLabel.Size = new System.Drawing.Size(20, 13);
            this.anLabel.TabIndex = 11;
            this.anLabel.Text = "An";
            // 
            // institutieLabel
            // 
            this.institutieLabel.AutoSize = true;
            this.institutieLabel.Location = new System.Drawing.Point(45, 170);
            this.institutieLabel.Name = "institutieLabel";
            this.institutieLabel.Size = new System.Drawing.Size(55, 13);
            this.institutieLabel.TabIndex = 9;
            this.institutieLabel.Text = "Institututie";
            // 
            // adresaLabel
            // 
            this.adresaLabel.AutoSize = true;
            this.adresaLabel.Location = new System.Drawing.Point(45, 129);
            this.adresaLabel.Name = "adresaLabel";
            this.adresaLabel.Size = new System.Drawing.Size(40, 13);
            this.adresaLabel.TabIndex = 7;
            this.adresaLabel.Text = "Adresa";
            // 
            // cnpLabel
            // 
            this.cnpLabel.AutoSize = true;
            this.cnpLabel.Location = new System.Drawing.Point(45, 98);
            this.cnpLabel.Name = "cnpLabel";
            this.cnpLabel.Size = new System.Drawing.Size(29, 13);
            this.cnpLabel.TabIndex = 5;
            this.cnpLabel.Text = "CNP";
            // 
            // numeLabel
            // 
            this.numeLabel.AutoSize = true;
            this.numeLabel.Location = new System.Drawing.Point(45, 28);
            this.numeLabel.Name = "numeLabel";
            this.numeLabel.Size = new System.Drawing.Size(35, 13);
            this.numeLabel.TabIndex = 3;
            this.numeLabel.Text = "Nume";
            // 
            // rolLabel
            // 
            this.rolLabel.AutoSize = true;
            this.rolLabel.Location = new System.Drawing.Point(45, 68);
            this.rolLabel.Name = "rolLabel";
            this.rolLabel.Size = new System.Drawing.Size(29, 13);
            this.rolLabel.TabIndex = 1;
            this.rolLabel.Text = "ROL";
            // 
            // tasks
            // 
            this.tasks.Controls.Add(this.listaTaskuri);
            this.tasks.Controls.Add(this.deleteTask);
            this.tasks.Controls.Add(this.adaugaComentarii);
            this.tasks.Controls.Add(this.comentarii);
            this.tasks.Controls.Add(this.notiteStudent);
            this.tasks.Controls.Add(this.checkBox1);
            this.tasks.Controls.Add(this.adaugaTask);
            this.tasks.Controls.Add(this.label10);
            this.tasks.Controls.Add(this.taskNou);
            this.tasks.Location = new System.Drawing.Point(4, 22);
            this.tasks.Name = "tasks";
            this.tasks.Padding = new System.Windows.Forms.Padding(3);
            this.tasks.Size = new System.Drawing.Size(429, 408);
            this.tasks.TabIndex = 0;
            this.tasks.Text = "Tasks";
            this.tasks.UseVisualStyleBackColor = true;
            // 
            // listaTaskuri
            // 
            this.listaTaskuri.FormattingEnabled = true;
            this.listaTaskuri.Location = new System.Drawing.Point(18, 23);
            this.listaTaskuri.Name = "listaTaskuri";
            this.listaTaskuri.Size = new System.Drawing.Size(139, 186);
            this.listaTaskuri.TabIndex = 9;
            this.listaTaskuri.SelectedIndexChanged += new System.EventHandler(this.listaTaskuri_SelectedIndexChanged);
            // 
            // deleteTask
            // 
            this.deleteTask.Location = new System.Drawing.Point(18, 235);
            this.deleteTask.Name = "deleteTask";
            this.deleteTask.Size = new System.Drawing.Size(83, 26);
            this.deleteTask.TabIndex = 8;
            this.deleteTask.Text = "Delete Task";
            this.deleteTask.UseVisualStyleBackColor = true;
            this.deleteTask.Click += new System.EventHandler(this.deleteTask_Click);
            // 
            // adaugaComentarii
            // 
            this.adaugaComentarii.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.adaugaComentarii.Location = new System.Drawing.Point(300, 272);
            this.adaugaComentarii.Name = "adaugaComentarii";
            this.adaugaComentarii.Size = new System.Drawing.Size(105, 31);
            this.adaugaComentarii.TabIndex = 7;
            this.adaugaComentarii.Text = "Adauga comentarii";
            this.adaugaComentarii.UseVisualStyleBackColor = true;
            this.adaugaComentarii.Click += new System.EventHandler(this.adaugaComentarii_Click);
            // 
            // comentarii
            // 
            this.comentarii.Location = new System.Drawing.Point(174, 168);
            this.comentarii.Multiline = true;
            this.comentarii.Name = "comentarii";
            this.comentarii.Size = new System.Drawing.Size(231, 87);
            this.comentarii.TabIndex = 6;
            // 
            // notiteStudent
            // 
            this.notiteStudent.Enabled = false;
            this.notiteStudent.Location = new System.Drawing.Point(174, 23);
            this.notiteStudent.Multiline = true;
            this.notiteStudent.Name = "notiteStudent";
            this.notiteStudent.Size = new System.Drawing.Size(232, 129);
            this.notiteStudent.TabIndex = 5;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(178, 330);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(86, 17);
            this.checkBox1.TabIndex = 4;
            this.checkBox1.Text = "Toti studentii";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // adaugaTask
            // 
            this.adaugaTask.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.adaugaTask.Location = new System.Drawing.Point(18, 356);
            this.adaugaTask.Name = "adaugaTask";
            this.adaugaTask.Size = new System.Drawing.Size(140, 27);
            this.adaugaTask.TabIndex = 3;
            this.adaugaTask.Text = "Adauga task";
            this.adaugaTask.UseVisualStyleBackColor = true;
            this.adaugaTask.Click += new System.EventHandler(this.Adauga_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(48, 303);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(85, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Numele task-ului";
            // 
            // taskNou
            // 
            this.taskNou.Location = new System.Drawing.Point(18, 330);
            this.taskNou.Name = "taskNou";
            this.taskNou.Size = new System.Drawing.Size(139, 20);
            this.taskNou.TabIndex = 1;
            // 
            // tasksTab
            // 
            this.tasksTab.Controls.Add(this.tasks);
            this.tasksTab.Controls.Add(this.usersTab);
            this.tasksTab.Controls.Add(this.rapoarteTab);
            this.tasksTab.Location = new System.Drawing.Point(178, 22);
            this.tasksTab.Multiline = true;
            this.tasksTab.Name = "tasksTab";
            this.tasksTab.SelectedIndex = 0;
            this.tasksTab.Size = new System.Drawing.Size(437, 434);
            this.tasksTab.TabIndex = 0;
            // 
            // rapoarteTab
            // 
            this.rapoarteTab.Controls.Add(this.raportTotiStudentii);
            this.rapoarteTab.Location = new System.Drawing.Point(4, 22);
            this.rapoarteTab.Name = "rapoarteTab";
            this.rapoarteTab.Size = new System.Drawing.Size(429, 408);
            this.rapoarteTab.TabIndex = 2;
            this.rapoarteTab.Text = "Rapoarte";
            // 
            // raportTotiStudentii
            // 
            this.raportTotiStudentii.Location = new System.Drawing.Point(130, 128);
            this.raportTotiStudentii.Name = "raportTotiStudentii";
            this.raportTotiStudentii.Size = new System.Drawing.Size(142, 52);
            this.raportTotiStudentii.TabIndex = 0;
            this.raportTotiStudentii.Text = "Raport toti sudentii";
            this.raportTotiStudentii.UseVisualStyleBackColor = true;
            this.raportTotiStudentii.Click += new System.EventHandler(this.raportTotiStudentii_Click);
            // 
            // DataSetPractica
            // 
            this.DataSetPractica.DataSetName = "DataSetPractica";
            this.DataSetPractica.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // DataTable1BindingSource
            // 
            this.DataTable1BindingSource.DataMember = "DataTable1";
            this.DataTable1BindingSource.DataSource = this.DataSetPractica;
            // 
            // DataTable1TableAdapter
            // 
            this.DataTable1TableAdapter.ClearBeforeFill = true;
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.Controls.Add(this.studentiDataGridView);
            this.panel1.Controls.Add(this.tasksTab);
            this.panel1.Location = new System.Drawing.Point(31, 45);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(624, 466);
            this.panel1.TabIndex = 2;
            // 
            // studentiDataGridView
            // 
            this.studentiDataGridView.AllowUserToAddRows = false;
            this.studentiDataGridView.AllowUserToDeleteRows = false;
            this.studentiDataGridView.AllowUserToResizeColumns = false;
            this.studentiDataGridView.AllowUserToResizeRows = false;
            this.studentiDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.studentiDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.studentiDataGridView.Location = new System.Drawing.Point(19, 22);
            this.studentiDataGridView.Name = "studentiDataGridView";
            this.studentiDataGridView.ReadOnly = true;
            this.studentiDataGridView.RowHeadersVisible = false;
            this.studentiDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.studentiDataGridView.Size = new System.Drawing.Size(157, 430);
            this.studentiDataGridView.TabIndex = 1;
            this.studentiDataGridView.SelectionChanged += new System.EventHandler(this.studentiDataGridView_SelectionChanged);
            // 
            // exportaUtilizatoriButton
            // 
            this.exportaUtilizatoriButton.Location = new System.Drawing.Point(263, 18);
            this.exportaUtilizatoriButton.Name = "exportaUtilizatoriButton";
            this.exportaUtilizatoriButton.Size = new System.Drawing.Size(117, 23);
            this.exportaUtilizatoriButton.TabIndex = 48;
            this.exportaUtilizatoriButton.Text = "Exporta utilizatori";
            this.exportaUtilizatoriButton.UseVisualStyleBackColor = true;
            this.exportaUtilizatoriButton.Click += new System.EventHandler(this.exportaUtilizatoriButton_Click);
            // 
            // importaUtilizatoriButton
            // 
            this.importaUtilizatoriButton.Location = new System.Drawing.Point(263, 68);
            this.importaUtilizatoriButton.Name = "importaUtilizatoriButton";
            this.importaUtilizatoriButton.Size = new System.Drawing.Size(117, 23);
            this.importaUtilizatoriButton.TabIndex = 49;
            this.importaUtilizatoriButton.Text = "Importa utilizatori";
            this.importaUtilizatoriButton.UseVisualStyleBackColor = true;
            this.importaUtilizatoriButton.Click += new System.EventHandler(this.importaUtilizatoriButton_Click);
            // 
            // IndrumatorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(678, 561);
            this.Controls.Add(this.panel1);
            this.MaximumSize = new System.Drawing.Size(800, 1000);
            this.MinimumSize = new System.Drawing.Size(650, 550);
            this.Name = "IndrumatorForm";
            this.Text = "Indrumator";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.IndrumatorForm_FormClosed);
            this.Load += new System.EventHandler(this.indrumatorForm_Load);
            this.usersTab.ResumeLayout(false);
            this.usersTab.PerformLayout();
            this.tasks.ResumeLayout(false);
            this.tasks.PerformLayout();
            this.tasksTab.ResumeLayout(false);
            this.rapoarteTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataSetPractica)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataTable1BindingSource)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.studentiDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.TabPage usersTab;
        public System.Windows.Forms.Button deleteStudent;
        public System.Windows.Forms.Button updateStudent;
        public System.Windows.Forms.Label parolaLabel;
        public System.Windows.Forms.TextBox parolaNoua;
        public System.Windows.Forms.TextBox mail;
        public System.Windows.Forms.TextBox institutie;
        public System.Windows.Forms.TextBox adresa;
        public System.Windows.Forms.TextBox cnp;
        public System.Windows.Forms.TextBox nume;
        public System.Windows.Forms.Label mailLabel;
        public System.Windows.Forms.Button adaugaStudent;
        public System.Windows.Forms.Label notaLabel;
        public System.Windows.Forms.Label anLabel;
        public System.Windows.Forms.Label institutieLabel;
        public System.Windows.Forms.Label adresaLabel;
        public System.Windows.Forms.Label cnpLabel;
        public System.Windows.Forms.Label numeLabel;
        public System.Windows.Forms.Label rolLabel;
        public System.Windows.Forms.TabPage tasks;
        public System.Windows.Forms.TabControl tasksTab;
        public System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox taskNou;
        public System.Windows.Forms.CheckBox checkBox1;
        public System.Windows.Forms.Button adaugaTask;
        public System.Windows.Forms.Button adaugaComentarii;
        public System.Windows.Forms.TextBox comentarii;
        public System.Windows.Forms.TextBox notiteStudent;
        public System.Windows.Forms.TabPage rapoarteTab;
        public System.Windows.Forms.BindingSource DataTable1BindingSource;
        public DataSetPractica DataSetPractica;
        public DataSetPracticaTableAdapters.DataTable1TableAdapter DataTable1TableAdapter;
        public System.Windows.Forms.Button raportTotiStudentii;
        public System.Windows.Forms.Button deleteTask;
        private System.Windows.Forms.ListBox listaTaskuri;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox rolUtilizatorComboBox;
        private System.Windows.Forms.ComboBox notaComboBox;
        private System.Windows.Forms.ComboBox anComboBox;
        private System.Windows.Forms.DataGridView studentiDataGridView;
        private System.Windows.Forms.Button importaUtilizatoriButton;
        private System.Windows.Forms.Button exportaUtilizatoriButton;
    }
}