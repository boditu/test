﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Xml;
using System.Web;
using System.Web.Script.Serialization;


namespace practica
{
    public partial class IndrumatorForm : Form
    {
        public IndrumatorForm()
        {
            InitializeComponent();
        }

        private void button_adauga_student_Click(object sender, EventArgs e)
        {
            string rol="";
            if (rolUtilizatorComboBox.SelectedItem.ToString() == "indrumator")
            {
                rol = "1";
            }
            else rol = "2";

            long cnpInt=9;

            if  (parolaNoua.Text == "" || mail.Text == "")
            {
                MessageBox.Show("Introdu mail si parola !!");
            }
            else if (Int64.TryParse(cnp.Text, out cnpInt)) 
            {
                    Utilizator student = new Utilizator();
                    student.nume = nume.Text;
                    student.cnp = Convert.ToInt64(cnp.Text);
                    student.idRol = Convert.ToInt32(rol);
                    student.adresa = adresa.Text;
                    student.institutie = institutie.Text;
                    student.an = Convert.ToInt32(anComboBox.SelectedItem);
                    student.nota = Convert.ToInt32(notaComboBox.SelectedItem);
                    student.mail = mail.Text;
                    student.parola = parolaNoua.Text;
                    utile.AdaugaStudent(student);
                    parolaNoua.Text = "";

                    studentiDataGridView.DataSource = utile.DaTabelStudenti();
                
            }

            else 
                {
                    MessageBox.Show("Date introduse gresit !!");
                }                      
        }

        private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            Utilizator student = new Utilizator();
            student = utile.PopuleazaDetaliiUtilizator(Convert.ToInt32(studentiDataGridView.CurrentRow.Cells["idUtilizator"].Value));
            nume.Text = student.nume;
            rolUtilizatorComboBox.Text = student.idRol.ToString();
            adresa.Text = student.adresa;
            cnp.Text = student.cnp.ToString();
            institutie.Text = student.institutie;
            anComboBox.Text = student.an.ToString();
            notaComboBox.Text= student.nota.ToString();
            mail.Text = student.mail;
        }

        private void indrumatorForm_Load(object sender, EventArgs e)
        {
            studentiDataGridView.DataSource = utile.DaTabelStudenti();
            studentiDataGridView.Columns["idUtilizator"].Visible = false;
          
            Constante.listaUtilizatori.Add("indrumator");
            Constante.listaUtilizatori.Add("student");
            rolUtilizatorComboBox.DataSource = Constante.listaUtilizatori;
            for (int i =1; i < 11; i++)
            {
               Constante.listaNote.Add(i);
            }
            notaComboBox.DataSource = Constante.listaNote;
            for (int i = 1; i < 5; i++)
            {
                Constante.listaAn.Add(i);
            }
            anComboBox.DataSource = Constante.listaAn; 
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string rol = "";
            if (rolUtilizatorComboBox.SelectedItem.ToString() == "indrumator")
            {
                rol = "1";
            }
            else rol = "2";

            long cnpInt = 9;

            if (mail.Text == "")
            {
                MessageBox.Show("Introdu mail !!");
            }
            else if (Int64.TryParse(cnp.Text, out cnpInt))
            {
                Utilizator student = new Utilizator();
                student.idUtilizator = Convert.ToInt32(studentiDataGridView.CurrentRow.Cells["idUtilizator"].Value);
                student.nume = nume.Text;
                student.cnp = Convert.ToInt64(cnp.Text);
                student.idRol = Convert.ToInt32(rol);
                student.adresa = adresa.Text;
                student.institutie = institutie.Text;
                student.an = Convert.ToInt32(anComboBox.SelectedItem);
                student.nota = Convert.ToInt32(notaComboBox.SelectedItem);
                student.mail = mail.Text;
                student.parola = parolaNoua.Text;
                utile.UpdateStudent(student);
                parolaNoua.Text = "";
                studentiDataGridView.DataSource = utile.DaTabelStudenti();

            }

            else
            {
                MessageBox.Show("Date introduse gresit !!");
            }  
        }

        private void deleteStudent_Click(object sender, EventArgs e)
        {
            utile.StergeTaskurileStudentului(Convert.ToInt32(studentiDataGridView.CurrentRow.Cells["idUtilizator"].Value));
            utile.StergeStudent(nume.Text);
            studentiDataGridView.DataSource = utile.DaTabelStudenti();
        }

        private void Adauga_Click(object sender, EventArgs e)
        {
            
            utile.AdaugaTask(Convert.ToInt32(studentiDataGridView.CurrentRow.Cells["idUtilizator"].Value), taskNou.Text);
            listaTaskuri.DataSource = utile.DaTakurileStudentului(Convert.ToInt32(studentiDataGridView.CurrentRow.Cells["idUtilizator"].Value));
            taskNou.Text = "";
        }



        private void adaugaComentarii_Click(object sender, EventArgs e)
        {
            utile.AdaugaComentarii(comentarii.Text, Convert.ToInt32(studentiDataGridView.CurrentRow.Cells["idUtilizator"].Value), listaTaskuri.SelectedItem.ToString());
        }

        private void raportTotiStudentii_Click(object sender, EventArgs e)
        {
            RaportTotiSudentii raport = new RaportTotiSudentii();
            raport.ShowDialog();
        }

        private void deleteTask_Click(object sender, EventArgs e)
        {
            utile.StergeTask(listaTaskuri.SelectedItem.ToString());
            listaTaskuri.DataSource = utile.DaTakurileStudentului(Convert.ToInt32(studentiDataGridView.CurrentRow.Cells["idUtilizator"].Value));
        
        }

        private void listaStudenti_SelectedIndexChanged(object sender, EventArgs e)
        {

            listaTaskuri.DataSource = utile.DaTakurileStudentului(Convert.ToInt32(studentiDataGridView.CurrentRow.Cells["idUtilizator"].Value));
        }

        private void listaTaskuri_SelectedIndexChanged(object sender, EventArgs e)
        {
            Task task = new Task();
            task=utile.PopuleazaDetaliiTask(Convert.ToInt32(studentiDataGridView.CurrentRow.Cells["idUtilizator"].Value),listaTaskuri.SelectedItem.ToString());
            comentarii.Text= task.comentarii;
            notiteStudent.Text = task.notiteTask;
        }

        private void IndrumatorForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void studentiDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            Utilizator student = new Utilizator();
            student = utile.PopuleazaDetaliiUtilizator(Convert.ToInt32(studentiDataGridView.CurrentRow.Cells["idUtilizator"].Value));
            nume.Text = student.nume;
            rolUtilizatorComboBox.Text = student.idRol.ToString();
            adresa.Text = student.adresa;
            cnp.Text = student.cnp.ToString();
            institutie.Text = student.institutie;
            anComboBox.Text = student.an.ToString();
            notaComboBox.Text = student.nota.ToString();
            mail.Text = student.mail;
            listaTaskuri.DataSource = utile.DaTakurileStudentului(Convert.ToInt32(studentiDataGridView.CurrentRow.Cells["idUtilizator"].Value));
        }

        private void exportaUtilizatoriButton_Click(object sender, EventArgs e)
        {
            string jsonExport = utile.ConvertDataTabletoString();
            System.IO.File.WriteAllText(@"C:\Users\boditu\Desktop\Proba.json", jsonExport);
        }

        private void importaUtilizatoriButton_Click(object sender, EventArgs e)
        {
            string continutulFisierului = System.IO.File.ReadAllText(@"C:\Users\boditu\Desktop\Proba.json");
            string json = @"{""data"":" + continutulFisierului + "}";

            ListaUtilizatori utilizatori = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<ListaUtilizatori>(json);
            utile.AdaugaListaStudentiInBazaDeDate(utilizatori);
            studentiDataGridView.DataSource = utile.DaTabelStudenti();
        }
    }
}
