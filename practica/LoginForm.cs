﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace practica
{
    public partial class login : Form
    {
        public login()
        {
            InitializeComponent();
        }
        public void ResetLoginForm()
        {
            mailTextBox.Text = "";
            parolaTextBox.Text="";
        }

        private void exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ok_Click(object sender, EventArgs e)
        {
            if (mailTextBox.Text.Length == 0 || parolaTextBox.Text.Length == 0)
            {
                MessageBox.Show("Nume utilizator sau parola neintroduse");
            }
            else
            {
                if (utile.UtilizatorValid(mailTextBox.Text, utile.CripteazaParola(parolaTextBox.Text)))
                {
                    if (utile.TipUtilizator(mailTextBox.Text) == Constante.TipuriUtilizator.indrumator)
                    {
                        MessageBox.Show("Logare ca Indrumator cu succes");
                        IndrumatorForm indrumatorForm = new IndrumatorForm();
                        this.Hide();
                        indrumatorForm.Show();
                    }
                    else
                    {
                        utile.GasesteIdUtilizatorulDinMail(mailTextBox.Text);
                        MessageBox.Show("Logare ca Student cu succes");
                        StudentForm studentFrom = new StudentForm((Constante.studentLogat).ToString());
                        this.Hide();
                        studentFrom.Show();
                    }
                }
                else
                {
                    MessageBox.Show("Date intoduse gresit!");
                    ResetLoginForm();
                }
            }
        }
    }
}

