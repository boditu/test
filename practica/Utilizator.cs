﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practica
{
    public  class Utilizator
    {
        public int idUtilizator { get; set; }
        public string nume { get; set; }
        public int idRol { get; set; }
        public Int64 cnp { get; set; }
        public string adresa { get; set; }
        public string institutie { get; set; }
        public int an { get; set; }
        public string mail { get; set; }
        public string parola { get; set; }
        public int nota { get; set; }

    }
}
