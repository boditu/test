﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace practica
{
    public partial class RaportStudentCurentSaptamanal : Form
    {
        int id;
        DateTime oSaptamanaInUrma;
        public RaportStudentCurentSaptamanal()
        {
            InitializeComponent();
        }

        public RaportStudentCurentSaptamanal(int idCurent,DateTime timpSaptamana)
        {
            id = idCurent;
            oSaptamanaInUrma = timpSaptamana;
            InitializeComponent();
        }

        private void RaportStudentCurentSaptamanal_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'DataSetPractica.DataTable3' table. You can move, or remove it, as needed.
            this.DataTable3TableAdapter.Fill(this.DataSetPractica.DataTable3,id,oSaptamanaInUrma);

            this.reportViewer1.RefreshReport();
        }
    }
}
