﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practica
{
    class Task
    {
        public int idTask { get; set; }
        public int idUtilizator { get; set; }
        public string denumireTask { get; set; }
        public int idStatus { get; set; }
        public string durata { get; set; }
        public string notiteTask { get; set; }
        public string tipStatus { get; set; }
        public string comentarii { get; set; }
    }
}
