﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Diagnostics;

namespace practica
{
    static class utile
    {
        public static void AdaugaListaStudentiInBazaDeDate(ListaUtilizatori utilizatori)
        {
            foreach (var item in utilizatori.data)
            {
                Utilizator student = new Utilizator();
                student.nume = item.nume;
                student.parola = item.parola;
                student.institutie = item.institutie;
                student.an = item.an;
                student.cnp = item.cnp;
                student.mail = item.mail;
                student.nota = item.nota;
                student.idRol = item.idRol;
                student.adresa = item.adresa;

                utile.AdaugaStudent(student);
            }
        }
        public static string ConvertDataTabletoString()
        {
            DataTable dataTable = new DataTable();
            SqlConnection conexiune = utile.CreareConexiune();
            SqlCommand comandaSql = new SqlCommand(@"SELECT nume,adresa,idRol,cnp,institutie,an,mail,parola,nota
                                                        FROM utilizator", conexiune);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(comandaSql);
            dataAdapter.Fill(dataTable);
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dataRow in dataTable.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dataTable.Columns)
                {
                    row.Add(col.ColumnName, dataRow[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }


        public static void UpdateTimpTask(string numeTask,string durataTask)
        { 
            try
            {
                SqlConnection connection = utile.CreareConexiune();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                string query = @"update tasks set durata='" + durataTask + @"'
                                where denumireTask='" + numeTask+"'";
                //MessageBox.Show(query);
                command.CommandText = query;
                command.ExecuteNonQuery();
                utile.InchideConexiune(connection);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Eroare " + ex);
            }
        }
        public static string TimpInitial(string denumireTask)
        {
            string timpInitial=null;
            try
            {
                SqlConnection connection = utile.CreareConexiune();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                string query = @"select durata
                                from tasks
                                where denumireTask ='" + denumireTask+"'";
                command.CommandText = query;

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    timpInitial = reader["durata"].ToString();
                }
                utile.InchideConexiune(connection);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Eroare " + ex);
            }
            return timpInitial;
        }

        public static Task PopuleazaDetaliiTask(int studentLogat,string numeTask)
        {
            Task task = new Task(); ;
            try
            {
                SqlConnection connection = CreareConexiune();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                string query = @"select *
                                from status join tasks on status.idStatus=tasks.idStatus
                                Where tasks.denumireTask='" + numeTask + @"'
                                and idUtilizator=" + studentLogat;
                command.CommandText = query;
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    task.tipStatus = reader["tipStatus"].ToString();
                    task.notiteTask = reader["notiteTask"].ToString();
                    task.comentarii = reader["comentarii"].ToString();
                    task.durata = reader["durata"].ToString();
                }
                utile.InchideConexiune(connection);
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Eroare " + ex);
            }
            return task;            
        }
        
        
        public static void SchimbaStatus(string taskCurent,string statusCurent,int idUtilizator)
        {            
            try
            {
                int status;
                SqlConnection connection = utile.CreareConexiune();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;

                if (statusCurent == "Neinceput")
                { status = 1; }
                else if (statusCurent == "In lucru")
                { status = 2; }
                else { status = 3; }
                string query = "update tasks set idStatus='" + status + @"'
                                where denumireTask='" + taskCurent+@"'
                                and idUtilizator='"+idUtilizator+"'";
                //MessageBox.Show(query);
                command.CommandText = query;
                command.ExecuteNonQuery();
                utile.InchideConexiune(connection);
                MessageBox.Show("Status schimbat");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Eroare " + ex);
            }
        }
        
        public static void AdaugaNotiteTask(string notiteTask, int idStudent, string denumireTask)
        {
            try
            {
                SqlConnection connection = utile.CreareConexiune();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                string query = "update tasks set notiteTask='" + notiteTask + @"'
                                where denumireTask='" + denumireTask + @"'
                                and idUtilizator=" + idStudent;
                command.CommandText = query;
                command.ExecuteNonQuery();
                utile.InchideConexiune(connection);
                MessageBox.Show("Notita a fost adaugata");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Eroare " + ex);
            }
        }
        static public List<String> DaListaStatus()
        {
            SqlConnection conexiune = CreareConexiune();
            List<String> listaStatus = new List<string>();
            SqlDataReader listaStatusDataReader = (new SqlCommand(@"select tipStatus
                                                                from status", conexiune)).ExecuteReader();
            while (listaStatusDataReader.Read())
            {
                listaStatus.Add(listaStatusDataReader["tipStatus"].ToString());
            }
            InchideConexiune(conexiune);
            return listaStatus;
        }
        public static void StergeTaskurileStudentului(int idStudent)
        {
            try
            {
                SqlConnection connection = utile.CreareConexiune();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                string query = @"delete from tasks
                                where idUtilizator=" + idStudent;
                // MessageBox.Show(query);
                command.CommandText = query;
                command.ExecuteNonQuery();
                utile.InchideConexiune(connection);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Eroare " + ex);
            }
        }
        public static void StergeStudent(string nume)
        {
            try
            {
                SqlConnection connection = utile.CreareConexiune();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                string query = @"delete from utilizator
                                where nume='" + nume+"'";
                // MessageBox.Show(query);
                command.CommandText = query;
                command.ExecuteNonQuery();
                utile.InchideConexiune(connection);
                MessageBox.Show("S-a realizat stergerea");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Eroare " + ex);
            }
        }
        public static void UpdateStudent(Utilizator student)
        {

            try
            {
                SqlConnection connection = utile.CreareConexiune();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                String parola = student.parola;
                string query = "";
                if (parola == "")
                {
                    query = "update utilizator set nume='" + student.nume + "', idRol=" + student.idRol + " ,cnp=" + student.cnp + " ,adresa='" + student.adresa + "', institutie='" + student.institutie + "', an=" + student.an+ ", mail='" + student.mail + "' ,nota=" + student.nota + @"
                                where idUtilizator=" + student.idUtilizator;
                }
                else
                {
                    string hash = utile.CripteazaParola(parola);
                    query = "update utilizator set nume='" + student.nume + "', idRol=" + student.idRol + " ,cnp=" + student.cnp + " ,adresa='" + student.adresa + "', institutie='" + student.institutie + "', an=" + student.an + ", mail='" + student + "' ,parola='" + hash + "' ,nota=" +student.nota + @"
                                where idUtilizator=" + student.idUtilizator;
                }
                command.CommandText = query;
                command.ExecuteNonQuery();
                utile.InchideConexiune(connection);
                MessageBox.Show("S-a realizat update-ul");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Eroare " + ex);
            }
        }
        public static void AdaugaStudent(Utilizator student)
        {
            SqlConnection connection = utile.CreareConexiune();
            String parola = student.parola;
            string parolaCriptata = utile.CripteazaParola(parola);
            SqlCommand cmd = new SqlCommand(@"INSERT INTO utilizator (idRol, nume, cnp, adresa, institutie, an, nota, mail, parola)
                                                    VALUES  ('" + student.idRol + "','" + student.nume + "','" + student.cnp + "','" + student.adresa + "','" + student.institutie + "','" + student.an + "','" + student.nota + "','" + student.mail + "','" + parolaCriptata + "')", connection);
            cmd.ExecuteNonQuery();
            utile.InchideConexiune(connection);
            MessageBox.Show("Studentul a fost adaugat");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="numele utilizatorului"></param>
        /// <returns></returns>
        public static Utilizator PopuleazaDetaliiUtilizator(int idStudent)
        {
            Utilizator student = new Utilizator();
            try
            {
                SqlConnection connection = utile.CreareConexiune();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                string query = @"select * from utilizator
                                where idUtilizator ='" + idStudent + "'";
                command.CommandText = query;

                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    student.nume = reader["nume"].ToString();
                    student.idRol = Convert.ToInt32(reader["idRol"]);
                    student.cnp = Convert.ToInt64(reader["cnp"]);
                    student.adresa= reader["adresa"].ToString();
                    student.institutie= reader["institutie"].ToString();
                    student.an = Convert.ToInt32(reader["an"]);
                    student.mail = reader["mail"].ToString();
                    student.nota =Convert.ToInt32( reader["nota"]);
                }
                utile.InchideConexiune(connection);
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Eroare " + ex);
            }
            return student;
        }
        public static void AdaugaComentarii(string comentarii,int idStudent, string denumireTask)
        {
            try
            {
                SqlConnection connection = utile.CreareConexiune();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                string query = "update tasks set comentarii='" + comentarii+ @"'
                                where denumireTask='" + denumireTask + @"'
                                and idUtilizator=" + idStudent;
                //MessageBox.Show(query);
                command.CommandText = query;
                command.ExecuteNonQuery();
                utile.InchideConexiune(connection);
                MessageBox.Show("Comentariul a fost adaugat");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Eroare " + ex);
            }
        }

        public static DataTable DaTabelStudenti()
        {
            SqlConnection conexiune = utile.CreareConexiune();
            SqlCommand comandaSql = new SqlCommand(@"SELECT idUtilizator,nume
                                            FROM utilizator 
                                            WHERE idRol=2", conexiune);
            SqlDataReader studentiSelectatiDataReader = comandaSql.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(studentiSelectatiDataReader);
            utile.InchideConexiune(conexiune);
            return dataTable;
        }

        static public void AdaugaTask(int idStudent,string denumireTask)
        {
            SqlConnection conexiune = CreareConexiune();
            string durata = "00:00:00";
            SqlCommand cmd = new SqlCommand(@"INSERT INTO tasks (idUtilizator, denumireTask, idStatus, dataCrearii,durata)
                                                VALUES  (" + idStudent + ",'" + denumireTask + "'," + 1 + ",'" + GetTimeStamp(DateTime.Now) + "','" + durata + "')", conexiune);
            cmd.ExecuteNonQuery();
            InchideConexiune(conexiune);
           
        }
        static public void StergeTask(string denumireTask)
        {
            try
            {
                SqlConnection connection = utile.CreareConexiune();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                string query = @"delete from tasks
                                where denumireTask='" + denumireTask+"'";
                // MessageBox.Show(query);
                command.CommandText = query;
                command.ExecuteNonQuery();
                utile.InchideConexiune(connection);
                MessageBox.Show("S-a realizat stergerea");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Eroare " + ex);
            }

        }

        static public List<String> DaTakurileStudentului(int idUtilizator)
        {
            SqlConnection conexiune = CreareConexiune();
            List<String> listaTaskuri = new List<string>();
            SqlDataReader listaTaskuriDataReader = (new SqlCommand(@"select denumireTask
                                                                from tasks
                                                                where idUtilizator='" + idUtilizator + "'", conexiune)).ExecuteReader();
            while (listaTaskuriDataReader.Read())
            {
                listaTaskuri.Add(listaTaskuriDataReader["denumireTask"].ToString());
            }
            InchideConexiune(conexiune);
            return listaTaskuri;
        }

        static public List <String> DaListaUtizatorilor()
        {
            SqlConnection conexiune = CreareConexiune();
            List<String> listaUtilizatori = new List<string>();
            SqlDataReader listaUtilizatorilorDataReader = (new SqlCommand(@"select nume
                                                                from utilizator
                                                                where idRol=2", conexiune)).ExecuteReader();
            while (listaUtilizatorilorDataReader.Read())
            {
                listaUtilizatori.Add(listaUtilizatorilorDataReader["nume"].ToString());
            }
            InchideConexiune(conexiune);
            return listaUtilizatori;
        }

        internal static Constante.TipuriUtilizator TipUtilizator(string mailUtilizator)
        {
            SqlConnection conexiune = CreareConexiune();
            SqlCommand cmd = new SqlCommand(@"SELECT idRol
                                                FROM utilizator 
                                                WHERE mail='" + mailUtilizator + @"'", conexiune);
            SqlDataReader utilizatorDataReader = cmd.ExecuteReader();
            utilizatorDataReader.Read();
            int tipUtilizator = (int)utilizatorDataReader.GetInt32(0);
            InchideConexiune(conexiune);
            return (Constante.TipuriUtilizator)tipUtilizator;
        }

        public static int GasesteIdUtilizatorulDinMail(string mail)
        {
            SqlConnection conexiune = CreareConexiune();
            SqlCommand cmd = new SqlCommand(@"SELECT idUtilizator
                                                FROM utilizator 
                                                WHERE mail='" + mail+ @"'", conexiune);

            SqlDataReader idUtilizatorSelectat = cmd.ExecuteReader();
            idUtilizatorSelectat.Read();
            Constante.studentLogat = Convert.ToInt32(idUtilizatorSelectat["idUtilizator"]);
            InchideConexiune(conexiune);
            return Constante.studentLogat;
        }

        public static bool UtilizatorValid(string mail, string parola)
        {
            SqlConnection conexiune = CreareConexiune();
            mail = mail.Replace("'",@"\");
            Debug.Write(mail);
            SqlCommand cmd = new SqlCommand(@"SELECT count(1) 
                                                FROM utilizator 
                                                WHERE mail='" + mail + @"' 
                                                AND parola='" + parola + "'", conexiune);
            bool eUtilizatorValid = (int)cmd.ExecuteScalar() != 0;
            InchideConexiune(conexiune);
            return eUtilizatorValid;
        }

        public static string CripteazaParola(string input)
        {
            byte[] data = System.Text.Encoding.ASCII.GetBytes(input);
            data = new System.Security.Cryptography.SHA256Managed().ComputeHash(data);
            String parolaCriptata = System.Text.Encoding.ASCII.GetString(data);
            return parolaCriptata;
        }

        public static string GetTimeStamp(DateTime value)
        {
            return value.ToString("yyyy-MM-dd HH:mm:ss.fff");
        }
        public static SqlConnection CreareConexiune()
        {
            SqlConnection connection = new SqlConnection(@"Data Source=EVALD\SQLEXPRESS;Initial Catalog=practica;Integrated Security=True");
            connection.Open();
            return connection;
        }
        public static void InchideConexiune(SqlConnection connection)
        {
            connection.Close();
        }
    }
}
