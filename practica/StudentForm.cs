﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Diagnostics;
namespace practica
{
    public partial class StudentForm : Form
    {
        string mailUtilizatorCurent;
        Stopwatch timer = new Stopwatch();
        bool amTrecutDeLoad = false;
        public StudentForm()
        {
            InitializeComponent();
        }

        public StudentForm(string mailUtilizator)
        {
            mailUtilizatorCurent= mailUtilizator;
            InitializeComponent();
        }

        private void studentForm_Load(object sender, EventArgs e)
        {
            Utilizator student = new Utilizator();
            student = utile.PopuleazaDetaliiUtilizator(Constante.studentLogat);
            nume.Text = student.nume;
            adresa.Text = student.adresa;
            cnp.Text = student.cnp.ToString();
            institutie.Text = student.institutie;
            an.Text = student.an.ToString();
            nota.Text = student.nota.ToString();
            mail.Text = student.mail;
            listaTaskuri.DataSource=utile.DaTakurileStudentului(Constante.studentLogat);
            toateStatusurile.DataSource= utile.DaListaStatus();
            amTrecutDeLoad = true;
        }



        private void listaTask_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            timpActual.Text = "";
        }

        private void start_Click(object sender, EventArgs e)
        {
            timer.Start();
        }

        private void stop_Click(object sender, EventArgs e)
        {
            timer.Stop();
            TimeSpan ts = timer.Elapsed;
            string elapsedTime = ts.ToString(@"hh\:mm\:ss");
            timer.Reset();
            timpActual.Text = elapsedTime;
            string timpInitial = utile.TimpInitial(listaTaskuri.SelectedItem.ToString());
            string timpFinal = (TimeSpan.Parse(elapsedTime) + TimeSpan.Parse(timpInitial)).ToString();
            timpTotal.Text = timpFinal;
            utile.UpdateTimpTask(listaTaskuri.SelectedItem.ToString(),timpFinal);

        }

        private void adaugaTask_Click(object sender, EventArgs e)
        {
            utile.AdaugaTask(Constante.studentLogat,taskNou.Text);
            listaTaskuri.DataSource = utile.DaTakurileStudentului(Constante.studentLogat);
            taskNou.Text = "";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Utilizator student = new Utilizator();
            student.nume = nume.Text;
            student.idRol = 2;
            student.cnp = Convert.ToInt64(cnp.Text);
            student.adresa = adresa.Text;
            student.institutie = institutie.Text;
            student.an = Convert.ToInt32(an.Text);
            student.nota = Convert.ToInt32(nota.Text);
            student.mail = mail.Text;
            student.parola = parolaNoua.Text;
            student.idUtilizator = Constante.studentLogat;
            utile.UpdateStudent(student);
            parolaNoua.Text = "";
        }

        private void printeazaRaportSaptamanal_Click(object sender, EventArgs e)
        {
            DateTime timpActual = DateTime.Now;
            DateTime timpSaptamana = timpActual.AddDays(-7);
            RaportStudentCurentSaptamanal RaportStudentCurentSaptamanal = new RaportStudentCurentSaptamanal(Constante.studentLogat, timpSaptamana);
            RaportStudentCurentSaptamanal.ShowDialog();
        }

        private void printeazaRaportTotal_Click(object sender, EventArgs e)
        {
            RaportStudent RaportStudentCurent = new RaportStudent(Constante.studentLogat);
            RaportStudentCurent.ShowDialog();
        }

        private void adaugaNotite_Click(object sender, EventArgs e)
        {
            notiteTask.Text.Replace("'", @"\'");
            utile.AdaugaNotiteTask(notiteTask.Text,Constante.studentLogat, listaTaskuri.SelectedItem.ToString());
        }

        private void StudentForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void listaTaskuri_SelectedIndexChanged(object sender, EventArgs e)
        {
            Task task = new Task();
            task = utile.PopuleazaDetaliiTask(Constante.studentLogat, listaTaskuri.SelectedItem.ToString());
            status.Text = task.tipStatus;
            notiteTask.Text = task.notiteTask;
            comentarii.Text = task.comentarii;
            status.Text = task.tipStatus;
            timpTotal.Text = task.durata;
            timpActual.Text = "";
        }

        private void toateStatusurile_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!amTrecutDeLoad)
            {
                return;
            }
            utile.SchimbaStatus(listaTaskuri.SelectedItem.ToString(), toateStatusurile.SelectedItem.ToString(),Constante.studentLogat);
            status.Text = toateStatusurile.SelectedItem.ToString();
        } 
    }
}
